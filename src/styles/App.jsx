import styled from "styled-components";

const DivStyled = styled.div`
  max-width: 1300px;
  margin-left: 20vw;

  h2 {
    margin-top: 3.5rem;
    font-size: 1.75rem;
    font-weight: 500;
  }

  div.loading-spin {
    margin: 20vh 25vw;
    height: 120px;
    width: 120px;
    border: 8px solid rgba(221, 221, 221, 0.1);
    border-left-color: #0fbf6b;
    border-radius: 50%;
    animation: spin 0.8s linear infinite;
  }

  @keyframes spin {
    to {
      transform: rotate(360deg);
    }
  }
`;

const DivCardListStyled = styled.div`
  display: flex;
  flex-wrap: wrap;
  max-width: 1200px;
  margin-top: 1rem;
`;

export { DivStyled, DivCardListStyled };
