import styled from "styled-components";

const DivStyled = styled.div`
  display: flex;
  margin-top: 30px;

  button {
    background-color: #099c55;
    color: #fff;
    width: 80px;
    height: 35px;
    font-size: 16px;
  }
`;

export { DivStyled };
