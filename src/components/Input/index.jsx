import { Component } from "react";
import { DivStyled } from "./style";

class Input extends Component {
  render() {
    return (
      <DivStyled>
        <input
          type="text"
          placeholder="Filter by technologies"
          onChange={this.props.onChangeFunc}
        ></input>
        <button onClick={this.props.onCLickFunc}>Search</button>
      </DivStyled>
    );
  }
}

export default Input;
