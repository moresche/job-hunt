import { Component } from "react";
import { DivCardStyled } from "./style";
import UndefinedImg from "../../assets/undefined.png";

class Card extends Component {
  render() {
    const { job } = this.props;
    return (
      <>
        <DivCardStyled>
          <img
            src={job.company_logo ? job.company_logo : UndefinedImg}
            alt={job.company}
          />
          <h3>
            {job.company.length > 18
              ? `${job.company.substring(0, 18)}...`
              : job.company}
          </h3>
          <p>
            {job.title.length > 25
              ? `${job.title.substring(0, 22)}...`
              : job.title}
          </p>
          <span>
            {job.location.length > 28
              ? `${job.location.substring(0, 28)}...`
              : job.location}
          </span>
          <button
            onClick={() => {
              window.open(job.url);
            }}
          >
            Get Full Details
          </button>
        </DivCardStyled>
      </>
    );
  }
}

export default Card;
