import styled from "styled-components";

const DivCardStyled = styled.div`
  width: 250px;
  height: 250px;
  position: relative;
  justify-content: center;
  display: flex;
  align-items: center;
  flex-direction: column;
  background-color: #121212;
  margin-top: 3rem;
  border-radius: 20px;
  border: 1px solid #0fbf6b;
  margin-right: 2rem;

  img {
    height: 100px;
    width: 100px;
    border-radius: 50%;
    border: 2px solid #0fbf6b;
    position: absolute;
    top: -45px;
    object-fit: contain;
  }

  h3,
  span {
    margin-top: 2rem;
    text-align: center;
  }

  p {
    text-align: center;
    color: #0fbf6b;
  }

  span {
    margin-bottom: 2rem;
  }

  button {
    width: 130px;
    height: 40px;
    background-color: #099c55;
    border-radius: 5px;
    color: #f5f5f5;
  }

  button:hover {
    transition: 0.2s;
    filter: brightness(0.7);
  }
`;

export { DivCardStyled };
