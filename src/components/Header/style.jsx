import styled from "styled-components";

const HeaderStyled = styled.header`
  display: flex;
  max-width: 1300px;
  margin: 2rem auto;

  .img {
    width: 250px;
  }
`;

export { HeaderStyled };
