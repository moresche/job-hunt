import { Component } from "react";
import Logo from "../../assets/logo.png";
import { HeaderStyled } from "./style";

class Header extends Component {
  render() {
    return (
      <HeaderStyled>
        <img src={Logo} alt="JobHunt" />
      </HeaderStyled>
    );
  }
}

export default Header;
