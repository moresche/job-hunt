import { Component } from "react";
import { DivStyled, DivCardListStyled } from "./styles/App.jsx";
import Card from "./components/Card";
import Header from "./components/Header";
import Input from "./components/Input/";

class App extends Component {
  state = {
    jobs: [],
    loading: true,
    search: "",
    inputValue: "",
  };

  getJobs = () => {
    fetch(
      `https://jobhunt-api.herokuapp.com/jobs?description=${this.state.search}`
    )
      .then((response) => response.json())
      .then((response) => this.setState({ jobs: response, loading: false }));
  };

  componentDidMount() {
    this.getJobs();
  }

  getInputValue = (e) => this.setState({ inputValue: e.target.value });

  handleClick = (inputValue) => this.setState({ search: inputValue });

  componentDidUpdate(_, prevState) {
    if (this.state.search !== prevState.search) {
      this.getJobs();
    }
  }

  render() {
    const { jobs, loading, inputValue } = this.state;

    return (
      <DivStyled>
        <Header />
        <h2>🔥 Trending Jobs</h2>
        {loading && <div className="loading-spin" />}
        {!loading && (
          <Input
            onChangeFunc={(e) => this.getInputValue(e)}
            onCLickFunc={() => this.handleClick(inputValue)}
          />
        )}
        <DivCardListStyled>
          {jobs.map((job) => (
            <Card key={job.id} job={job} />
          ))}
        </DivCardListStyled>
      </DivStyled>
    );
  }
}

export default App;
